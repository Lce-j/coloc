export default () => (`
<div class="task">
    <div class="date">
        <label>Date</label>
        <input type="date" id="date"></input>
    </div>

    <div class="durer">
        <label>Durée</label>
        <input type="time" id="time"></input>
    </div>

    <div class="what">
        <label>Tache assignée</label>
        <input type="text" id="what"></input>
    </div>
    <div class ="description">
        <label>Description</label>
        <input type="text" id="description"></input>
    </div>
    <div class="money">
        <label>Budget</label>
        <input type="var" id="money"></input>
    </div>
    <div class="add">
        <button type="submit" class="submit">Ajouter</button>
    </div>
</div>
`);
