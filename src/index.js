import Router from './router';
import Coloc from './controllers/Coloc';

import './index.scss';

const routes = [{
  url: '/',
  controller: Coloc
}];

new Router(routes);
