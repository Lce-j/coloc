class Error04 {
  constructor() {
    this.el = document.querySelecto('#root');

    this.run();
  }

  render() {
    return '<h1>404</h1>';
  }

  run() {
    this.el.innerHTML = this.render();
  }
}

export default Error04;
