import ViewNav from '../views/nav';
import ViewLogin from '../views/login';
import ViewHomepage from '../views/homepage'; // Corrected path
import ViewCalendar from '../views/calendar';
import ViewTask from '../views/task';
import ViewCreation from '../views/creation';
import ViewInformation from '../views/information';
import ViewBudget from '../views/budget'; // Corrected path

class Coloc {
  constructor() {
    this.el = document.querySelector('#root');
    this.data = [];
    this.dataCalendar = [];
    this.db = [];
  }

  async postData(url = 'http://localhost/phpmyadmin/index.php?route=/database/structure&db=coloc', data = {}) {
    const response = await fetch(url, {
      method: 'POST',
      mode: 'cors',
      cache: 'no-cache',
      credentials: 'same-origin',
      headers: {
        'Content-Type': 'application/json'
      },
      redirect: 'follow',
      referrerPolicy: 'no-referrer',
      body: JSON.stringify(data)
    });
    return response.json();
  }

  async fetchData(url = 'http://localhost/phpmyadmin/index.php?route=/database/structure&db=coloc') {
    const response = await fetch(url, {
      credentials: 'same-origin'
    });
    return response.json();
  }

  render() {
    return `${ViewNav()}${ViewLogin()}`;
  }

  run() {
    this.el.innerHTML = this.render();
  }

  async login() {
    const email = document.querySelector('#email').value;
    const password = document.querySelector('#password').value;

    try {
      const dbResponse = await this.fetchData('http://localhost/phpmyadmin/index.php?route=/sql&pos=0&db=coloc&table=users');
      const founduser = dbResponse.find((user) => user.email === email
      && user.password === password);

      if (founduser) {
        sessionStorage.setItem('clé', 'valeur');
        document.getElementById('log-in').innerHTML = '<a>Identifiant correct, Vous êtes connecté</a>';
        this.el.innerHTML = `${ViewHomepage()}${ViewCalendar()}`;
      } else {
        document.getElementById('log-in').innerHTML = '<a>L\'email ou le mot de passe est incorrect. Veuillez réessayer</a>';
      }
    } catch (error) {
      // console.error('Error logging in:', error);
    }
  }

  setupEventListeners() {
    document.querySelector('#login-button').addEventListener('click', () => this.login());
    // Add other event listeners similarly
  }

  nav(event) {
    switch (event.target.id) {
      case 'task':
        this.el.innerHTML = ViewCreation();
        break;
      case 'budget':
        this.el.innerHTML = ViewBudget();
        break;
      case 'reglages':
        this.el.innerHTML = ViewInformation();
        break;
      default:
        break;
    }
  }

  async calendar() {
    const script = document.createElement('script');
    script.src = 'https://cdn.jsdelivr.net/npm/fullcalendar@6.1.14/index.global.min.js';
    document.head.appendChild(script);
    script.onload = () => {
      this.el.innerHTML = ViewCalendar();
    };
  }

  task(event) {
    if (event.target.id === 'asign') {
      if (this.id.user === this.task.user) {
        this.el.innerHTML = `${ViewTask()}${ViewHomepage('tache')}`;
      }
    } else if (event.target.id === 'delete') {
      // Handle delete task
    }
  }

  creation() {
    const {
      date, time, what, description, money
    } = this.input;
    const {
      date: dbDate, time: dbTime, what: dbWhat, description: dbDescription, money: dbMoney
    } = this.database;
    // const database = this.database;

    if (!date) {
      document.getElementById('date').innerHTML = '<a>Vous devez remplir la date</a>';
    } else if (!time) {
      document.getElementById('durer').innerHTML = '<a>Vous devez remplir la durée</a>';
    } else if (!what) {
      document.getElementById('what').innerHTML = '<a>Vous devez remplir la taches</a>';
    } else if (!description) {
      document.getElementById('description').innerHTML = '<a>Vous devez remplir la description</a>';
    } else if (!money) {
      document.getElementById('money').innerHTML = '<a>Vous devez remplir le budget</a>';
    } else if (date === dbDate
        && time === dbTime.time
        && what === dbWhat.what
        && description === dbDescription.description
        && money === dbMoney.money) {
      this.el.innerHTML = `${ViewTask()}${ViewCalendar()}`;
    }
  }

  async budget() {
    try {
      const budgetData = await this.fetchData('http://localhost/phpmyadmin/index.php?route=/sql&pos=0&db=coloc&table=budget');
      if (budgetData) {
        this.el.innerHTML = ViewBudget();
      }
    } catch (error) {
      // console.error('Error fetching budget data:', error);
    }
  }

  async information() {
    try {
      const userData = await this.fetchData('http://localhost/phpmyadmin/index.php?route=/sql&pos=0&db=coloc&table=users');
      if (userData) {
        this.el.innerHTML = ViewInformation();
      }
    } catch (error) {
      // console.error('Error fetching user information:', error);
    }
  }

  logout() {
    sessionStorage.removeItem('clé');
    document.write('Vous avez été déconnecté');
    this.el.innerHTML = ViewLogin();
  }
}

export default Coloc;
